TARGET = target

CC = gcc

SRC_PATH = src
BUILD_PATH = build
BIN_PATH = $(BUILD_PATH)/bin

SRC_EXT = c

SOURCES = $(shell find $(SRC_PATH) -name '*.$(SRC_EXT)' | sort -k 1nr | cut -f2-)
OBJECTS = $(SOURCES:$(SRC_PATH)/%.$(SRC_EXT)=$(BUILD_PATH)/%.o)

COMPILE_FLAGS = -Wall -Wextra -g
CFLAGS = $(COMPILE_FLAGS)
INCLUDES = -I include/

all: build
	@echo "Build successful."

build: $(BIN_PATH)/$(TARGET)
	@rm -f $(TARGET)
	@ln -s $(BIN_PATH)/$(TARGET) $(TARGET)

clean:
	@echo "Deleting $(TARGET) symlink"
	@rm -f $(TARGET)
	@echo "Cleaning directories"
	@rm -f $(BUILD_PATH)/*.o $(BUILD_PATH)/*.d
	@rm -f $(BIN_PATH)/*

$(BIN_PATH)/$(TARGET): $(OBJECTS)
	@echo "Linking: $@"
	$(CC) $(OBJECTS) -o $@

$(BUILD_PATH)/%.o: $(SRC_PATH)/%.$(SRC_EXT)
	@echo "Compiling: $< -> $@"
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

.PHONY: all run build clean
