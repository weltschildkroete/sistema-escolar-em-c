# Sistema Escolar em C

Projeto para VE de Laboratório de Programação I. Implementa em C um sistema de matrículas e disciplinas de uma instituição de ensino.

Um repositório de git contendo o código-fonte deste software pode ser encontrado no [Gitlab](https://gitlab.com/weltschildkroete/sistema-escolar-em-c).

## Compilando a partir do código-fonte

Em um sistema operacional do tipo Unix, equipado com [GCC](https://gcc.gnu.org/) e [GNU Make](https://www.gnu.org/software/make/), basta emitir o comando

```bash
make
```

dentro do diretório onde se encontra o arquivo `Makefile`. Caso seja necessário trocar o compilador de C, para, por exemplo, `clang`, basta modificar a linha do arquivo `Makefile` que possui a variável `CC` para o caminho do compilador de escolha. Isto é, se seu compilador está localizado em `/usr/bin/clang`, basta fazer

```
CC = /usr/bin/clang
```

Se tudo ocorrer sem erros, aparecerá na tela a mensagem "Build successful." Neste caso, o arquivo executável, chamado, no caso default, de `target`, se encontrará no mesmo diretório onde o comando de `make` foi originalmente emitido.

### Dependências

Esse projeto depende da library `argp.h` para fazer a análise sintática dos argumentos da linha de comando, logo tal lib será necessária para compilar este projeto a partir do código-fonte.

## Uso

O programa manterá um banco de dados, no caso default, no arquivo `build/db.txt`. Nesse arquivo, estarão guardados, por período, uma lista de alunos, uma lista de disciplinas, e uma lista de quais alunos estão matriculados em quais disciplinas. O arquivo executável deve ser chamado com dois argumentos principais: o período em que iremos operar e uma ação a ser executada.

### Exemplos

Para adicionar um aluno na lista de alunos do período de 2020.1, isto é, o primeiro período de 2020, basta emitir o comando

```bash
./target --period=2020.1 insert student
```

Em seguida, um menu irá requisitar as informações necessárias para registrar o aluno. Para adicionar uma disciplina, basta mudar `student` para `subject`.

Para remover um aluno ou disciplina, basta utilizar mudar `insert` para `remove`, e inserir o código do aluno ou disciplina a ser removido. Por exemplo, emitindo o comando

```bash
./target -p 2020.1 remove student
```

aparecerá, em seguida, um menu que pedirá as informações necessárias para realizar essa operação.

Para registrar um aluno em uma disciplina, basta utilizar a ação `register`

```bash
./target -p 2020.1 register
```

e inserir os códigos do aluno e da disciplina. Para desfazer essa operação, utiliza-se a ação `unregister`.

Para checar as disciplinas em que um aluno está matriculado, pode-se utilizar a ação `check`, da seguinte forma

```bash
./target -p 2020.1 check student
```

Para checar os alunos matriculados em uma determinada disciplina, basta trocar `student` por `subject`. Finalmente, para checar a lista de todos os alunos registrados em um período, pode-se utilizar o modificador `all`, isto é

```bash
./target -p 2020.1 check student all
```

Equivalentemente, trocando `student` por `subject`, pode-se checar a lista de todas as disciplinas registradas em um período.

Se desejarmos utilizar outro arquivo como banco de dados, basta utilizar o argumento `--output`, ou `-o`. Por exemplo, para utilizar o arquivo `banco_de_dados.txt` em vez do default, utilizamos o comando

```bash
./target -o banco_de_dados.txt -p 2020.1 check student all
```
