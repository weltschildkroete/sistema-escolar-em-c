#pragma once

#include "insert.h"
#include "remove.h"
#include "print.h"

void clean_stdin()
{
	char c;
	do {
        	c = getchar();
	} while (c != '\n' && c != EOF);
}

int check_student(struct period *per)
{
	struct student *stu;
	int code;

	printf("Checking subjects of a given student in semester %s.\n", per->name);

	printf("Enter the student's code: ");
	scanf("%d", &code);
	stu = period_find_student_by_code(per, code);
	if (stu == NULL) {
		printf("No such student with code %d.\n", code);
		return 1;
	}

	printf("Printing subjects that student %s is enrolled in:\n", stu->name);
	period_print_subjects_of_student(per, stu->code);

        return 0;
}

int check_subject(struct period *per)
{
        struct subject *sub;
	int code;

        printf("Checking students enrolled in a given subject in semester %s.\n", per->name);

	printf("Enter the subject's code: ");
	scanf("%d", &code);
	sub = period_find_subject_by_code(per, code);
	if (sub == NULL) {
		printf("No such subject with code %d.\n", code);
		return 1;
	}

	printf("Printing students that enrolled in subject %s:\n", sub->name);
	period_print_students_in_subject(per, sub->code);

        return 0;
}

int check_student_all(struct period *per)
{
        printf("Printing all students in semester %s:\n", per->name);

	period_print_students(per);

	return 0;
}

int check_subject_all(struct period *per)
{
        printf("Printing all subjects in semester %s:\n", per->name);

	period_print_subjects(per);

        return 0;
}

int insert_student(struct period *per)
{
	struct student stu;

        printf("Registering a student in semester %s.\n", per->name);

	printf("Code: ");
	scanf("%d", &stu.code);
	if (stu.code > 99999) {
		printf("Student's code must contain no more than 5 digits.\n");
		return 1;
	}
	if (period_find_student_by_code(per, stu.code) != NULL) {
		printf("Student with code %d already exists.\n", stu.code);
		return 1;
	}

	printf("Name: ");
	clean_stdin();
	scanf("%[^\n]", stu.name);
	if (strchr(stu.name, ',')) {
		printf("Student's name must not contain commas.\n");
		return 1;
	}

	printf("CPF: ");
	scanf("%ld", &stu.cpf);

	period_insert_student(per, stu);

	printf("Registration successful.\n");

        return 0;
}

int insert_subject(struct period *per)
{
	struct subject sub;

	printf("Register subject in semester %s.\n", per->name);

	printf("Code: ");
	scanf("%d", &sub.code);
	if (sub.code > 9999) {
		printf("Subject's code must contain no more than 4 digits.\n");
		return 1;
	}
	if (period_find_subject_by_code(per, sub.code) != NULL) {
		printf("Subject with code %d already exists.\n", sub.code);
		return 1;
	}

	printf("Name: ");
	clean_stdin();
	scanf("%[^\n]", sub.name);
	if (strchr(sub.name, ',')) {
		printf("Subject's name must not contain commas.\n");
		return 1;
	}

	printf("Teacher: ");
	clean_stdin();
	scanf("%[^\n]", sub.teacher);
	if (strchr(sub.teacher, ',')) {
		printf("Teacher's name must not contain commas.\n");
		return 1;
	}

	printf("Credits: ");
	scanf("%d", &sub.credits);

	period_insert_subject(per, sub);

	printf("Registration successful.\n");

	return 0;
}

int register_student_in_subject(struct period *per)
{
	struct tuple tup;
	int stu_code, sub_code;

	printf("Registering a given student in a given subject in semester %s.\n", per->name);

	printf("Student's code: ");
	scanf("%d", &stu_code);
	tup.stu = period_find_student_by_code(per, stu_code);
	if (tup.stu == NULL) {
		printf("No such student with code %d.\n", stu_code);
		return 1;
	}

	printf("Subject's code: ");
	scanf("%d", &sub_code);
	tup.sub = period_find_subject_by_code(per, sub_code);
	if (tup.sub == NULL) {
		printf("No such subject with code %d.\n", sub_code);
		return 1;
	}

	if (period_find_tuple_by_code(per, stu_code, sub_code) != NULL) {
		printf("Student %s already registered in subject %s.\n", tup.stu->name, tup.sub->name);
		return 1;
	}

	period_insert_tuple(per, tup);

	printf("Registration successful.\n");

	return 0;
}

int remove_student(struct period *per)
{
	int code;

	printf("Unregistering a given student from semester %s.\n", per->name);

	printf("Enter the student's code: ");
	scanf("%d", &code);

	if (period_remove_student_by_code(per, code) != 0) {
		printf("No such student with code %d.\n", code);
		return 1;
	}

	printf("Removal successful.\n");

	return 0;
}

int remove_subject(struct period *per)
{
	int code;

	printf("Unregistering a given subject from semester %s.\n", per->name);

	printf("Enter the subject's code: ");
	scanf("%d", &code);

	if (period_remove_subject_by_code(per, code) != 0) {
		printf("No such subject with code %d.\n", code);
		return 1;
	}

	printf("Removal successful.\n");

	return 0;
}

int unregister_student_from_subject(struct period *per)
{
	struct tuple tup;
	int stu_code, sub_code;

	printf("Unregistering a given student from a given subject in semester %s.\n", per->name);

	printf("Student's code: ");
	scanf("%d", &stu_code);
	tup.stu = period_find_student_by_code(per, stu_code);
	if (tup.stu == NULL) {
		printf("No such student with code %d.\n", stu_code);
		return 1;
	}

	printf("Subject's code: ");
	scanf("%d", &sub_code);
	tup.sub = period_find_subject_by_code(per, sub_code);
	if (tup.sub == NULL) {
		printf("No such subject with code %d.\n", sub_code);
		return 1;
	}

	if(period_unregister_student_from_subject_by_code(per, stu_code, sub_code) != 0) {
		printf("Student %s was/is not registered in subject %s.\n", tup.stu->name, tup.sub->name);
		return 1;
	}

	printf("Operation successful.\n");

	return 0;
}
