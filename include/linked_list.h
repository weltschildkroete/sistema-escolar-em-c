#pragma once

#include <stdlib.h>
#include <string.h>

struct node {
	void *data;
	struct node *next;
};

void *list_insert(struct node **head, void *data, int data_size)
{
        struct node *new = malloc(sizeof(struct node));
        new->data = malloc(data_size);
        new->next = *head;

        memcpy(new->data, data, data_size);

        *head = new;

        return new->data;
}

void list_remove(struct node **head, void *data)
{
        struct node *ptr = *head;

        if (ptr->data == data) {
                *head = ptr->next;
                free(ptr->data);
                free(ptr);
                return;
        }

        while (ptr->next != NULL) {
                if (ptr->next->data == data) {
                        struct node *link = ptr->next;
                        ptr->next = link->next;
                        free(link->data);
                        free(link);
                        return;
                }
                ptr = ptr->next;
        }
}

void *list_find(struct node **head, int (*equals)(void *, void *), void *key)
{
        struct node *ptr = *head;

        while (ptr != NULL) {
                if (equals(ptr->data, key)) {
                        return ptr->data;
                }
                ptr = ptr->next;
        }

        return NULL;
}

void list_foreach(struct node **head, void (*fptr)(void *))
{
        struct node *ptr = *head;

        while (ptr != NULL) {
                fptr(ptr->data);
                ptr = ptr->next;
        }
}

void list_free(struct node *head)
{
        struct node* ptr;

        while (head != NULL) {
                ptr = head;
                head = head->next;
                free(ptr->data);
                free(ptr);
        }
}
