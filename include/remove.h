#pragma once

#include "linked_list.h"
#include "types.h"
#include "search.h"

int period_remove_student_by_code(struct period *per, int code)
{
	struct student *stu = period_find_student_by_code(per, code);

	if (stu == NULL) {
		return 1;
	}

	struct node *ptr = per->tuples;
	while (ptr != NULL) {
		struct tuple *tup = ptr->data;
		struct node *next = ptr->next;
		if (tup->stu->code == code) {
			list_remove(&per->tuples, tup);
		}
		ptr = next;
	}

	list_remove(&per->students, stu);

	return 0;
}

int period_remove_subject_by_code(struct period *per, int code)
{
	struct subject *sub = period_find_subject_by_code(per, code);

	if (sub == NULL) {
		return 1;
	}

	struct node *ptr = per->tuples;
	while (ptr != NULL) {
		struct tuple *tup = ptr->data;
		struct node *next = ptr->next;
		if (tup->sub->code == code) {
			list_remove(&per->tuples, tup);
		}
		ptr = next;
	}

	list_remove(&per->subjects, sub);

	return 0;
}

int period_unregister_student_from_subject_by_code(struct period *per, int stu_code, int sub_code)
{
	struct tuple *tup = period_find_tuple_by_code(per, stu_code, sub_code);

	if (tup == NULL) {
		return 1;
	}

	list_remove(&per->tuples, tup);

	return 0;
}

void periods_free(struct node *periods)
{
	struct node *ptr = periods;

	while (ptr != NULL) {
		struct period *per = ptr->data;
		list_free(per->tuples);
		list_free(per->subjects);
		list_free(per->students);
		ptr = ptr->next;
	}

	list_free(periods);
}
