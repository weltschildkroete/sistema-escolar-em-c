#pragma once

#include "linked_list.h"
#include "types.h"

void period_insert_student(struct period *per, struct student stu)
{
	list_insert(&per->students, &stu, sizeof(struct student));
}

void period_insert_subject(struct period *per, struct subject sub)
{
	list_insert(&per->subjects, &sub, sizeof(struct subject));
}

void period_insert_tuple(struct period *per, struct tuple tup)
{
	list_insert(&per->tuples, &tup, sizeof(struct tuple));
}

struct period *new_period(struct node **periods, char *name)
{
	struct period per;
	strcpy(per.name, name);
	per.students = NULL;
	per.subjects = NULL;
	per.tuples = NULL;

	return list_insert(periods, &per, sizeof(struct period));
}
