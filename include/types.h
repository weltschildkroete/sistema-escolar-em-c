#pragma once

struct student {
	int code;
	char name[128];
	long int cpf;
};

struct subject {
	int code;
	char name[128];
	char teacher[128];
	int credits;
};

struct tuple {
	struct student *stu;
	struct subject *sub;
};

struct period {
	char name[7];
	struct node *students;
	struct node *subjects;
	struct node *tuples;
};

enum operation {
	CHECK_STUDENT,
	CHECK_SUBJECT,
	CHECK_STUDENT_ALL,
	CHECK_SUBJECT_ALL,
	INSERT_STUDENT,
	INSERT_SUBJECT,
	REGISTER,
	REMOVE_STUDENT,
	REMOVE_SUBJECT,
	UNREGISTER,
};

struct config {
	char file_name[16];
	char period[7];
	enum operation op;
};
