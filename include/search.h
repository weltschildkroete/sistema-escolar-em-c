#pragma once

#include <string.h>

#include "linked_list.h"
#include "types.h"

int by_student_code(void *data, void *key)
{
	return ((struct student *) data)->code == *((int *) key);
}

int by_subject_code(void *data, void *key)
{
	return ((struct subject *) data)->code == *((int *) key);
}

int by_tuple(void *data, void *key)
{
	return (((struct tuple *) data)->stu->code == ((int *) key)[0] &&
		((struct tuple *) data)->sub->code == ((int *) key)[1]);
}

int by_period_name(void *data, void *key)
{
	return !strcmp(((struct period *) data)->name, (char *) key);
}

struct student *period_find_student_by_code(struct period *per, int code)
{
	return list_find(&per->students, by_student_code, &code);
}

struct subject *period_find_subject_by_code(struct period *per, int code)
{
	return list_find(&per->subjects, by_subject_code, &code);
}

struct tuple *period_find_tuple_by_code(struct period *per, int stu_code, int sub_code)
{
	int keys[2] = { stu_code, sub_code };
	return list_find(&per->tuples, by_tuple, keys);
}

struct period *periods_find_period_by_name(struct node *periods, char name[7])
{
	return list_find(&periods, by_period_name, name);
}
