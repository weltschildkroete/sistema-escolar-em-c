#pragma once

#include <stdio.h>

#include "insert.h"
#include "linked_list.h"
#include "types.h"

void write_student(FILE *fp, struct student *stu)
{
        fprintf(fp, "1, %d, %s, %ld\n", stu->code, stu->name, stu->cpf);
}

void write_subject(FILE *fp, struct subject *sub)
{
        fprintf(fp, "2, %d, %s, %s, %d\n", sub->code, sub->name, sub->teacher, sub->credits);
}

void write_tuple(FILE *fp, struct tuple *tup)
{
        fprintf(fp, "3, %d, %d\n", tup->stu->code, tup->sub->code);
}

void write_period(FILE *fp, struct period *per)
{
        fprintf(fp, "0, %s\n", per->name);

        struct node *stu_ptr = per->students;
        while (stu_ptr != NULL) {
                struct student *stu = stu_ptr->data;
                write_student(fp, stu);
                stu_ptr = stu_ptr->next;
        }

        struct node *sub_ptr = per->subjects;
        while (sub_ptr != NULL) {
                struct subject *sub = sub_ptr->data;
                write_subject(fp, sub);
                sub_ptr = sub_ptr->next;
        }

        struct node *tup_ptr = per->tuples;
        while (tup_ptr != NULL) {
                struct tuple *tup = tup_ptr->data;
                write_tuple(fp, tup);
                tup_ptr = tup_ptr->next;
        }
}

void save(struct config config, struct node *periods)
{
        FILE *fp;
        struct node *ptr = periods;

        fp = fopen(config.file_name, "w");

        while (ptr != NULL) {
                struct period *per = ptr->data;
                write_period(fp, per);
                ptr = ptr->next;
        }

        fclose(fp);
}

void load_student(FILE **fp, struct period *per)
{
        struct student stu;

        fscanf(*fp, "%d, %[^,], %ld\n", &stu.code, stu.name, &stu.cpf);

	period_insert_student(per, stu);
}

void load_subject(FILE **fp, struct period *per)
{
        struct subject sub;

        fscanf(*fp, "%d, %[^,], %[^,], %d\n", &sub.code, sub.name, sub.teacher, &sub.credits);

	period_insert_subject(per, sub);
}

void load_tuple(FILE **fp, struct period *per)
{
        struct tuple tup;
	int stu_code, sub_code;

        fscanf(*fp, "%d, %d\n", &stu_code, &sub_code);

	tup.stu = period_find_student_by_code(per, stu_code);
	tup.sub = period_find_subject_by_code(per, sub_code);

	period_insert_tuple(per, tup);
}

struct period *load_period(FILE **fp, struct node **periods)
{
        char name[7];

        fscanf(*fp, "%s\n", name);

        return new_period(periods, name);
}

void load(struct config config, struct node **periods)
{
        FILE *fp;
        int type;
        struct period *per;

        fp = fopen(config.file_name, "r");

        if (fp == NULL) {
                return;
        }

        int error = 0;
        while (!feof(fp) && !error) {
                fscanf(fp, "%d, ", &type);
                switch (type) {
                case 0:
                        per = load_period(&fp, periods);
                        break;
                case 1:
                        load_student(&fp, per);
                        break;
                case 2:
                        load_subject(&fp, per);
                        break;
                case 3:
                        load_tuple(&fp, per);
                        break;
                default:
                        printf("Error parsing the file %s. Couldn't load previous settings.", config.file_name);
                        error = 1;
                }
        }

        fclose(fp);
}
