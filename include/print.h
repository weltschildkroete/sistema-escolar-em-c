#pragma once

#include <stdio.h>

#include "linked_list.h"
#include "types.h"

void print_student(void *data)
{
	struct student *stu = data;
	printf("> Student %d:\n\tName: %s\n\tCPF: %ld\n", stu->code, stu->name, stu->cpf);
}

void print_subject(void *data)
{
	struct subject *sub = data;
	printf("> Subject %d:\n\tName: %s\n\tTeacher: %s\n\tCredits: %d\n", sub->code, sub->name, sub->teacher, sub->credits);
}

void print_tuple(void *data)
{
	struct tuple *tup = data;
	printf("(%s, %s)\n", tup->stu->name, tup->sub->name);
}

void print_period(void *data)
{
	struct period *per = data;
	printf("%s\n", per->name);
}

void period_print_students(struct period *per)
{
	list_foreach(&per->students, print_student);
}

void period_print_subjects(struct period *per)
{
	list_foreach(&per->subjects, print_subject);
}

void period_print_tuples(struct period *per)
{
	list_foreach(&per->tuples, print_tuple);
}

void periods_print(struct node *periods)
{
	list_foreach(&periods, print_period);
}

void periods_print_students(struct node *periods)
{
	struct node *ptr = periods;

	while (ptr != NULL) {
		struct period *per = ptr->data;
		period_print_students(per);
		ptr = ptr->next;
	}
}

void periods_print_subjects(struct node *periods)
{
	struct node *ptr = periods;

	while (ptr != NULL) {
		struct period *per = ptr->data;
		period_print_subjects(per);
		ptr = ptr->next;
	}
}

void periods_print_tuples(struct node *periods)
{
	struct node *ptr = periods;

	while (ptr != NULL) {
		struct period *per = ptr->data;
		period_print_tuples(per);
		ptr = ptr->next;
	}
}

void period_print_subjects_of_student(struct period *per, int code)
{
	struct node *ptr = per->tuples;
	while (ptr != NULL) {
		struct tuple *tup = ptr->data;
		if (tup->stu->code == code) {
			print_subject(tup->sub);
		}
		ptr = ptr->next;
	}
}

void period_print_students_in_subject(struct period *per, int code)
{
	struct node *ptr = per->tuples;
	while (ptr != NULL) {
		struct tuple *tup = ptr->data;
		if (tup->sub->code == code) {
			print_student(tup->stu);
		}
		ptr = ptr->next;
	}
}
