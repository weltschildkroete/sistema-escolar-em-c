#include <string.h>
#include <argp.h>

#include "api.h"
#include "io.h"

static char doc[] = "Command-line utility for managing students and subjects. \v";

static char args_doc[] = "OP [ARGS...]";

static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	struct config *config = state->input;

	switch (key) {
	case 'o':
		strcpy(config->file_name, arg);
		break;
	case 'p':
		strcpy(config->period, arg);
		break;
	case ARGP_NO_ARGS:
		argp_usage(state); // Exit
		break;
	case ARGP_KEY_ARG: {
		char **argv = state->argv;
		int next = state->next;

		if (!strcmp(arg, "check")) {
			if (argv[next] == NULL) {
				argp_usage(state); // EXIT
			}

			if (!strcmp(argv[next], "student")) {
				char *mod = argv[next + 1];
				if (mod != NULL && !strcmp(mod, "all")) {
					config->op = CHECK_STUDENT_ALL;
				} else {
					config->op = CHECK_STUDENT;
				}
			} else if (!strcmp(argv[next], "subject")) {
				char *mod = argv[next + 1];
				if (mod != NULL && !strcmp(mod, "all")) {
					config->op = CHECK_SUBJECT_ALL;
				} else {
					config->op = CHECK_SUBJECT;
				}
			}
		} else if (!strcmp(arg, "insert")) {
			if (argv[next] == NULL) {
				argp_usage(state); // EXIT
			}

			if (!strcmp(argv[next], "student")) {
				config->op = INSERT_STUDENT;
			} else if (!strcmp(argv[next], "subject")) {
				config->op = INSERT_SUBJECT;
			}
		} else if (!strcmp(arg, "register")) {
			config->op = REGISTER;
		} else if (!strcmp(arg, "remove")) {
			if (argv[next] == NULL) {
				argp_usage(state); // EXIT
			}

			if (!strcmp(argv[next], "student")) {
				config->op = REMOVE_STUDENT;
			} else if (!strcmp(argv[next], "subject")) {
				config->op = REMOVE_SUBJECT;
			}
		} else if (!strcmp(arg, "unregister")) {
			config->op = UNREGISTER;
		} else {
			argp_usage(state); // Exit
		}

		state->next = state->argc;
		break;
	}
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static struct argp_option options[] = {
	{ "output", 'o', "FILE", 0, "Write and read from this file", 0 },
	{ "period", 'p', "PERIOD", 0, "Period to check or modify, in the format YYYY.P", 0 },
	{ 0 },
};

static struct argp argp = { options, parse_opt, args_doc, doc, NULL, NULL, NULL };

int main(int argc, char **argv)
{
	int ret;

	struct node *periods = NULL;

	struct config config;
	strcpy(config.file_name, "build/db.txt");
	config.period[0] = 0;

	argp_parse(&argp, argc, argv, 0, 0, &config);

	if (strlen(config.period) == 0) {
		printf("The argument 'period' is mandatory.\n");
		return 1;
	}

	load(config, &periods);

	struct period *per = periods_find_period_by_name(periods, config.period);

	if (per == NULL) {
		per = new_period(&periods, config.period);
	}

	switch (config.op) {
	case CHECK_STUDENT:
		ret = check_student(per);
		break;
	case CHECK_SUBJECT:
		ret = check_subject(per);
		break;
	case CHECK_STUDENT_ALL:
		ret = check_student_all(per);
		break;
	case CHECK_SUBJECT_ALL:
		ret = check_subject_all(per);
		break;
	case INSERT_STUDENT:
		ret = insert_student(per);
		break;
	case INSERT_SUBJECT:
		ret = insert_subject(per);
		break;
	case REGISTER:
		ret = register_student_in_subject(per);
		break;
	case REMOVE_STUDENT:
		ret = remove_student(per);
		break;
	case REMOVE_SUBJECT:
		ret = remove_subject(per);
		break;
	case UNREGISTER:
		ret = unregister_student_from_subject(per);
		break;
	}

	if (ret == 0) {
		save(config, periods);
	}

	periods_free(periods);

	return ret;
}
